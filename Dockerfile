FROM golang:1.20.4-alpine as build

WORKDIR /app

ENV GOPROXY=https://goproxy.cn
ENV TZ="Asia/Shanghai"

COPY . .

RUN go mod download

# 指定main包的根函数
RUN go build -o main ./main.go

EXPOSE 7400

CMD ["./main"]
