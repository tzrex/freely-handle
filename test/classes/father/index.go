package father

import "fmt"

type ISource interface {
	SourceSave() string
}

type SourceStu struct {
	Msg string
	Mod IModel
}

func (s *SourceStu) SourceSave() string {
	fmt.Println("s.Mod", s.Mod)

	table := s.Mod.TableName()
	msg := s.Msg + "<==>" + table
	return msg
}

type IModel interface {
	TableName() string
}

type ModelStu struct {
	Name string
}

func (m *ModelStu) TableName() string {
	return m.Name
}
