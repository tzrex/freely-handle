package sourcefront

import (
	"freely/bin/comm"
	"freely/bin/crud"
	modelfront "freely/src/model/front"
)

type SourceFrontUsers struct {
	*crud.Source
}

/**
 * 用户详情
 */
func (s *SourceFrontUsers) SourceInfo(id uint) (interface{}, error) {
	var table = comm.Db.GetDB(nil)
	var userInfo = &modelfront.FrontUserStu{}
	query := table.Where("id=?", id).
		Omit("created_at", "updated_at", "password").
		First(userInfo)
	if query.Error != nil {
		return nil, query.Error
	}
	query = table.Table(modelfront.FrontRoleName).
		Where("id=?", userInfo.RoleID).
		Omit("created_at", "updated_at").
		First(&userInfo.Role)
	if query.Error != nil {
		return nil, query.Error
	}
	query = table.Table(modelfront.FrontDeptName).
		Where("id=?", userInfo.DepartmentID).
		Omit("created_at", "updated_at").
		First(&userInfo.Department)
	if query.Error != nil {
		return nil, query.Error
	}

	return userInfo, nil
}

/**
 * 用户数据实例
 */
func NewSourceFrontUsers() *SourceFrontUsers {
	return &SourceFrontUsers{
		&crud.Source{
			Database:   comm.Db,
			Model:      &modelfront.FrontUserStu{},
			ModelName:  modelfront.FrontUserName,
			FieldOmits: []string{"password"},
			FiledTypes: modelfront.FrontUserCloumnTypeMap,
		},
	}
}
