package crud

import (
	"freely/bin/customType"

	"gorm.io/gorm"
)

type IModel interface {
	TableName() string
	GroupName() string
}

// 因为gorm自带的model返回的时间类型格式不规范，所以这里自己重新定义一下
type BaseModel struct {
	ID        uint                   `gorm:"primarykey" json:"id"`
	CreatedAt *customType.CustomTime `gorm:"comment:创建时间" json:"createdAt,omitempty"`
	UpdatedAt *customType.CustomTime `gorm:"comment:更新时间" json:"updatedAt,omitempty"`
	DeletedAt gorm.DeletedAt         `gorm:"index" json:"-"`
}

// 初始化BaseModel。（没什么用，仅占位）
func NewBaseModel() *BaseModel {
	return &BaseModel{}
}
