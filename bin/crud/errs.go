package crud

import "errors"

var (
	ErrCrudModelNil = errors.New("model.is.nil")
	ErrCrudIdType   = errors.New("id.err")
)
