package constant

// redis 相关的变量
const (
	RedisOnlineUsers = "online_users"
)

// 中间件
const (
	AuthFromTo   = "From-To"
	AuthUserID   = "userId"
	AuthUsername = "username"
	Author       = "Authorization"
	AuthRole     = "userRole"
	AuthRoleID   = "userRoleId"
)
