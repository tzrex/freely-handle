package before

import (
	basemiddle "freely/bin/baseMiddle"

	"github.com/gin-contrib/pprof"
	"github.com/gin-gonic/gin"
)

// 初始化Gin的参数
type GinSchema struct {
	IsProd        bool   // 是否是生产环境
	SessionKey    string // session的键
	SessionSecret string // session的唯一值
}

/**
 * 需要判断redis中的token的hash值，所以需要传入RedisClient对象
 */
func CreateGin(config *GinSchema) *gin.Engine {
	isProd := config.IsProd
	if isProd {
		gin.SetMode(gin.ReleaseMode)
	}

	Gin := gin.New()

	if !isProd {
		Gin.Use(gin.Logger())
	}

	Gin.Use(basemiddle.ExceptionHandler())
	Gin.Use(basemiddle.LimitRequest())
	Gin.Use(basemiddle.CorsHandle([]string{"*"}))
	Gin.Use(basemiddle.I18nHandle())

	pprof.Register(Gin)

	return Gin
}
