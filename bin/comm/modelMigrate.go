package comm

import (
	"freely/bin/crud"
)

// 迁移失败的表
var ModelMigrateBad = []string{}

// 表数据结构迁移
func MigrateTable(model crud.IModel) {
	err := Mysql.AutoMigrate(model)
	if err != nil {
		name := model.TableName()
		ModelMigrateBad = append(ModelMigrateBad, name)
	}
}
