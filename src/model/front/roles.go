package modelfront

import (
	"freely/bin/comm"
	"freely/bin/crud"
)

/**
 * 角色表：和菜单表多对多
 */
type FrontRoleStu struct {
	crud.BaseModel
	Name    string          `gorm:"type:varchar(40)" json:"name" validate:"required,stand"`
	Label   string          `gorm:"type:varchar(30);unique" json:"label" validate:"required,letter"`
	MenuIds []uint          `gorm:"-" json:"menuIds,omitempty"` // 存入中间表的数据的ID
	Menus   []*FrontMenuStu `gorm:"-" json:"menus,omitempty"`   // 存入中间表的数据
}

// 角色表字段属性
var FrontRoleColTypeMap = map[string]string{
	"name":  crud.FiledLike,
	"label": crud.FiledLike,
}

var FrontRoleName = "front_role"

func (m *FrontRoleStu) TableName() string {
	return FrontRoleName
}

func (m *FrontRoleStu) GroupName() string {
	return ModelName
}

// 迁移用户的角色表结构
func MigrateFrontRole() {
	comm.MigrateTable(&FrontRoleStu{})
}
