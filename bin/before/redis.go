package before

import (
	"context"
	"time"

	"github.com/redis/go-redis/v9"
)

var AppCtx = context.TODO()

// redis 连接配置
type RedisConnetConfigStu struct {
	RedisHost   string
	RedisPort   string
	RedisPass   string
	DialTimeout time.Duration
	ReadTimeout time.Duration
}

/**
 * 连接Redis数据库
 */
func NewConnectionRedis(config *RedisConnetConfigStu) *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:        config.RedisHost + ":" + config.RedisPort,
		Password:    config.RedisPass,
		DialTimeout: config.DialTimeout,
		ReadTimeout: config.ReadTimeout,
	})
}
