package basemiddle

import (
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

/**
 * 配置请求头，允许跨域访问
 */
func CorsHandle(origins []string) gin.HandlerFunc {
	corsConfig := cors.Config{
		AllowOrigins:     origins,
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type"},
		AllowCredentials: false,
		MaxAge:           24 * time.Hour,
	}
	return cors.New(corsConfig)
}
