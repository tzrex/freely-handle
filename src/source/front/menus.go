package sourcefront

import (
	"freely/bin/comm"
	"freely/bin/crud"
	modelfront "freely/src/model/front"
)

type SourceFrontMenus struct {
	*crud.Source
}

func NewSourceFrontMenus() *SourceFrontMenus {
	return &SourceFrontMenus{
		&crud.Source{
			ModelName:  modelfront.FrontMenuName,
			Model:      &modelfront.FrontMenuStu{},
			Database:   comm.Db,
			FiledTypes: modelfront.FrontMenuColTypeMap,
			// FieldOmits: ,
		},
	}
}
