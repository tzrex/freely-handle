package child

import "freely/test/classes/father"

type BaseModelStu struct {
	*father.ModelStu
}

func NewBaseModelStu() *BaseModelStu {
	return &BaseModelStu{
		&father.ModelStu{
			Name: "base",
		},
	}
}

type BaseSourceStu struct {
	*father.SourceStu
}

var baseModelClass = NewBaseModelStu()

func NewBaseSourceStu() *BaseSourceStu {
	return &BaseSourceStu{
		&father.SourceStu{
			Msg: "base.sources",
			Mod: baseModelClass,
		},
	}
}
