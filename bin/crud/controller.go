package crud

import (
	"fmt"
	"freely/bin/before"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

const (
	RouterGet    = "GET"
	RouterPost   = "POST"
	RouterPut    = "PUT"
	RouterDelete = "DELETE"
	RouterPatch  = "PATCH"
)

type IController interface {
	Save(*gin.Context, bool) // 新增和修改
	Del(*gin.Context)
	Info(*gin.Context)
	Table(*gin.Context, bool) // 分页查询和全部查询
}

type delParam struct {
	Ids []uint `json:"ids"`
}

type RouterItem struct {
	From string
	Call func(ctx *gin.Context)
}

type Controller struct {
	Prefix string
	Module string
	Source ISource
}

// 初始化Controller实例
func NewController(pre string, mod string, source ISource) *Controller {
	return &Controller{
		Prefix: pre,
		Module: mod,
		Source: source,
	}
}

// 增加和修改
func (c *Controller) Save(ctx *gin.Context, isAdd bool) {
	if objIsNil(c.Source, ctx) {
		return
	}

	params := map[string]any{}

	body := ctx.Request.Body
	fmt.Println(body)

	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, before.ReqFail(ctx, err.Error()))
		return
	}

	var res interface{}
	uid, err := c.Source.SourceSave(params, isAdd)

	if err != nil {
		ctx.JSON(http.StatusOK, before.ReqFail(ctx, err.Error()))
		return
	}

	if isAdd {
		res = uid
	} else {
		res = "修改成功"
	}

	ctx.JSON(http.StatusOK, before.ReqOk(ctx, res))
}

// 删除
func (c *Controller) Del(ctx *gin.Context) {
	if objIsNil(c.Source, ctx) {
		return
	}

	stu := delParam{}

	if err := ctx.ShouldBindJSON(&stu); err != nil {
		ctx.JSON(http.StatusBadRequest, before.ReqFail(ctx, err.Error()))
		return
	}

	if len(stu.Ids) == 0 {
		// 等于零直接返回正确
		ctx.JSON(http.StatusOK, before.ReqOk(ctx, 0))
		return
	}
	rows, err := c.Source.SourceDel(stu.Ids)
	if err != nil {
		ctx.JSON(http.StatusOK, before.ReqFail(ctx, err.Error()))
		return
	}
	ctx.JSON(http.StatusOK, before.ReqOk(ctx, rows))
}

// 详情
func (c *Controller) Info(ctx *gin.Context) {
	if objIsNil(c.Source, ctx) {
		return
	}

	strId := ctx.Query("id")
	if strId == "" {
		ctx.JSON(http.StatusBadRequest, before.ReqFail(ctx, ErrCrudIdType.Error()))
		return
	}
	id, err := strconv.ParseUint(strId, 10, 64)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, before.ReqFail(ctx, ErrCrudIdType.Error()))
		return
	}
	res, err := c.Source.SourceInfo(uint(id))
	if err != nil {
		ctx.JSON(http.StatusOK, before.ReqFail(ctx, err.Error()))
		return
	}
	ctx.JSON(http.StatusOK, before.ReqOk(ctx, res))
}

// 列表
func (c *Controller) Table(ctx *gin.Context, isPage bool) {
	if objIsNil(c.Source, ctx) {
		return
	}

	allQuery := ctx.Request.URL.Query()
	pageQuery := allQuery.Get("page")
	sizeQuery := allQuery.Get("size")
	order := allQuery.Get("order")
	sort := allQuery.Get("sort")

	allQuery.Del("page")
	allQuery.Del("size")
	allQuery.Del("order")
	allQuery.Del("sort")

	var page = 1
	var size = 10
	if pageQuery != "" {
		num, err := strconv.Atoi(pageQuery)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, before.ReqFail(ctx, "page.type.err"))
			return
		}
		page = num
	}
	if sizeQuery != "" {
		num, err := strconv.Atoi(sizeQuery)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, before.ReqFail(ctx, "size.type.err"))
			return
		}
		size = num
	}

	var params = &BaseSearch{
		Page:  page,
		Size:  size,
		Order: order,
		Sort:  sort,
	}

	for key, val := range allQuery {
		query := &QueryFild{Field: key, Value: val[0]}
		params.FiledsParam = append(params.FiledsParam, query)
	}

	var res interface{}
	var err error

	if isPage {
		res, err = c.Source.SourcePage(params)
	} else {
		res, err = c.Source.SourceList(params)
	}

	if err != nil {
		ctx.JSON(http.StatusOK, before.ReqFail(ctx, err.Error()))
		return
	}
	ctx.JSON(http.StatusOK, before.ReqOk(ctx, res))
}

// func makeParams(ctx *gin.Context) *BaseSearch {
// 	var params = &BaseSearch{}
// 	var equal = ctx.QueryArray("equal")
// 	var like = ctx.QueryArray("like")
// 	var less = ctx.QueryArray("less")
// 	var greater = ctx.QueryArray("greater")
// }

func objIsNil(model ISource, ctx *gin.Context) bool {
	var isNil = model == nil
	if isNil {
		ctx.JSON(http.StatusOK, before.ReqFail(ctx, ErrCrudModelNil.Error()))
		return true
	}
	return false
}

func RegisterController(group *gin.RouterGroup, c IController, apis []string, extendRouter map[string]*RouterItem, middle ...func(ctx *gin.Context)) error {
	if len(middle) > 0 {
		for _, fun := range middle {
			if fun != nil { // 避免犯浑导致的错误
				group.Use(fun)
			}
		}
	}

	var baseApiMap = map[string]*RouterItem{
		"/add":    {RouterPost, func(ctx *gin.Context) { c.Save(ctx, true) }},
		"/update": {RouterPost, func(ctx *gin.Context) { c.Save(ctx, false) }},
		"/delete": {RouterPost, c.Del},
		"/page":   {RouterGet, func(ctx *gin.Context) { c.Table(ctx, true) }},
		"/list":   {RouterGet, func(ctx *gin.Context) { c.Table(ctx, false) }},
		"/info":   {RouterGet, c.Info},
	}

	var loopRouter = func(routes map[string]*RouterItem) {
		for api, route := range routes {
			switch route.From {
			case RouterGet:
				group.GET(api, route.Call)
			case RouterPost:
				group.POST(api, route.Call)
			case RouterPut:
				group.PUT(api, route.Call)
			case RouterDelete:
				group.DELETE(api, route.Call)
			default:
				group.OPTIONS(api, route.Call)
			}
		}
	}
	// 基础路由
	if len(apis) > 0 {
		var apiMap = make(map[string]*RouterItem, len(apis))
		for _, api := range apis {
			var apiType = baseApiMap[api]
			if apiType != nil {
				apiMap[api] = baseApiMap[api]
			}
		}
		loopRouter(apiMap)
	}

	// 自定义路由
	loopRouter(extendRouter)

	return nil
}
