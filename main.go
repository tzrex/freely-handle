package main

import (
	"freely/bin/comm"
	"freely/src/model"
	"freely/src/router"
)

func main() {
	defer func() {
		comm.RedisCli.Close()
	}()

	model.ModelMigrate()
	router.RegisterRouters()

	comm.Gin.Run(":7400")
}
