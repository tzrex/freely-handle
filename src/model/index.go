package model

import modelfront "freely/src/model/front"

/**
 * 迁移表结构
 * 推荐只在开发环境下使用
 */
func ModelMigrate() {
	// front 的表结构
	modelfront.MigrateFrontDept()
	modelfront.MigrateFrontUsers()
	modelfront.MigrateFrontRole()
	modelfront.MigrateFrontMenu()
	modelfront.MigrateFrontRoleMenus()
}
