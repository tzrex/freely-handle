package before

import (
	"github.com/gin-contrib/i18n"
	"github.com/gin-gonic/gin"
)

type BaseReq struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

var (
	// 一切正常
	CodeSuccess = 1000
	// 出现错误
	CodeFail = 1001
	CodeErr  = 2001
)

/**
 * @Param ctx *gin.Context 对应请求的上下文
 * @Param data any 需要返回的自定义数据
 */
func ReqOk(ctx *gin.Context, data interface{}) *BaseReq {
	return &BaseReq{
		Code: CodeSuccess,
		Msg:  i18n.MustGetMessage(ctx, "base.success"),
		Data: data,
	}
}

/**
 * @Param ctx *gin.Context 对应请求的上下文
 * @Param msgID string 对应的错误信息的在 i8n 中的消息ID
 */
func ReqFail(ctx *gin.Context, msgID string) *BaseReq {
	return &BaseReq{
		Code: CodeFail,
		Msg:  i18n.MustGetMessage(ctx, "base.error"),
		Data: i18n.MustGetMessage(ctx, msgID),
	}
}
