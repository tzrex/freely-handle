package router

import (
	"freely/bin/before"
	"freely/bin/comm"
	"freely/src/controller"
	"net/http"

	"github.com/gin-contrib/i18n"
	"github.com/gin-gonic/gin"
)

func RegisterRouters() {
	indexRouter() // 添加一个默认路由
	controller.RegisterController()
}

func indexRouter() {
	comm.Gin.GET("/", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, before.ReqOk(ctx, i18n.MustGetMessage(ctx, "base.welcome")))
	})
}
