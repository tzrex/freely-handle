package modelfront

import "freely/bin/comm"

/**
 * 角色与菜单的中间表，以角色为主
 */
type FrontRoleMenus struct {
	RoleID uint `gorm:"type:varchar(80);index:role_menu_id;unique" json:"roleId"`
	MenuID uint `gorm:"type:varchar(80);index:role_menu_id;unique" json:"menuId"`
}

var FrontRoleMenusName = "front_role_menus"

// 表名
func (m *FrontRoleMenus) TableName() string {
	return FrontRoleMenusName
}

// 模块名
func (m *FrontRoleMenus) GroupName() string {
	return ModelName
}

// 迁移角色与菜单的中间表
func MigrateFrontRoleMenus() {
	comm.MigrateTable(&FrontRoleMenus{})
}
