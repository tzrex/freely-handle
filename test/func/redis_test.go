package test

import (
	"freely/bin/comm"
	"testing"
)

func TestRedis(t *testing.T) {
	res, err := comm.RedisCli.Get(comm.AppCtx, "one").Result()
	if err != nil {
		t.Log("err", err)
	}
	t.Log("res==>", res)
}
