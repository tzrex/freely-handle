package test

import (
	"freely/src/methods"
	"testing"
	"time"
)

func TestFunc(t *testing.T) {
	// var data = &methods.JwtData{
	// 	UserID:   1,
	// 	Username: "Rex",
	// 	RoleID:   1,
	// 	Role:     "admin",
	// 	LoginAt:  "admin",
	// }

	// token, err := methods.TokenCreate(data)
	// if err != nil {
	// 	t.Log("err==>", err)
	// 	t.FailNow()
	// }

	// t.Log("[ token ]: ", token)
	token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
		"eyJ1c2VySWQiOjEsInVzZXJuYW1lIjoiUmV4Iiwicm9sZUlkIjoxLCJyb2xlIjoiYWRtaW4iLCJsb2dpbkF0IjoiYWRtaW4iLCJpc3MiOiJSZXgiLCJuYmYiOjE3MDYwMDIwNzh9." +
		"QdBSt77fcnQ4Qtftaww6jHGuQsac9Fvs6qm9ShR3Mpk"

	for i := 0; i < 4; i++ {
		time.Sleep(time.Second * 5)
		parseToken, err := methods.TokenParse(token)
		if err != nil {
			t.Log("[ parse ]: ", err)
		} else {
			t.Log("parseToken==>", parseToken)
		}
	}
}
