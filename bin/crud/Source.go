package crud

import (
	"reflect"
	"strings"

	"freely/bin/before"
	"freely/bin/utils"

	"gorm.io/gorm"
)

type ISource interface {
	SourceSave(map[string]any, bool) (uint, error)
	SourceDel([]uint) (int64, error)
	SourceInfo(id uint) (interface{}, error)
	SourcePage(*BaseSearch) (interface{}, error)
	SourceList(*BaseSearch) (interface{}, error)
	GetModelName() string
	GetModel() IModel
}

const (
	FiledEqual   = "equal"   // 等于
	FiledLike    = "like"    // 模糊查询
	FiledLess    = "less"    // 小于
	FiledGreater = "greater" // 大于
)

const (
	BasePageNum   = 1
	BasePageSize  = 10
	BaseListLimit = 1000
)

type PageResult struct {
	Page *BasePage   `json:"page"`
	List interface{} `json:"list"`
}

/**
 * ModelName
 * Model
 * Database
 */
type Source struct {
	ModelName  string
	Model      IModel
	Database   before.IDatabaseConnect
	FiledTypes map[string]string // 字段类型
	FieldOmits []string
}

/**
 * 新增和修改
 */
func (s *Source) SourceSave(data map[string]any, isAdd bool) (uint, error) {
	var table = s.Database.GetDB(nil)
	var query *gorm.DB

	var resId uint = 0
	modelName := s.GetModelName()

	if modelName == "" {
		return resId, nil
	}

	if isAdd {
		delete(data, "id")
		query = table.Table(modelName).Omit("id").Create(data)
	} else {
		ID, ok := data["id"].(float64) // map默认为对应数据的最大类型
		if !ok || ID == 0 {
			return 0, ErrCrudIdType
		}
		resId = uint(ID)
		query = table.Table(modelName).Where("id=?", ID).Omit("id").Updates(data)
	}

	if query.Error != nil {
		return 0, query.Error
	}

	return resId, nil
}

/**
 * 删除
 */
func (s *Source) SourceDel(ids []uint) (int64, error) {
	if len(ids) == 0 {
		return 0, nil
	}

	if s.Model == nil {
		return 0, ErrCrudModelNil
	}

	query := s.Database.GetDB(nil).Where("id IN ?", ids).Delete(s.Model)
	if query.Error != nil {
		return 0, query.Error
	}

	return query.RowsAffected, nil
}

/**
 * 详情（各自实例的详情需自行实现）
 */
func (s *Source) SourceInfo(id uint) (interface{}, error) {
	return nil, nil
}

/**
 * 分页
 */
func (s *Source) SourcePage(search *BaseSearch) (interface{}, error) {
	if search.Page == 0 {
		search.Page = BasePageNum
	}

	if search.Size == 0 {
		search.Size = BasePageSize
	}

	if s.Model == nil || s.ModelName == "" {
		return nil, ErrCrudModelNil
	}

	var model = reflect.TypeOf(s.Model)
	sliceT := reflect.SliceOf(model)
	list := reflect.MakeSlice(sliceT, 0, 0).Interface()

	offset := (search.Page - 1) * search.Size

	m := s.Database.GetDB(nil).Table(s.ModelName)
	wh := SourceSearchWhere(m, search, s.FiledTypes)
	fields := SourceFieldsFiltter(wh, s.FieldOmits)
	query := fields.Limit(search.Size).Offset(offset).Find(&list)

	if query.Error != nil {
		return nil, query.Error
	}

	var result = &PageResult{
		Page: &BasePage{
			PageSize: search.Page,
			PageNum:  search.Size,
		},
		List: list,
	}

	query = SourceSearchWhere(m, search, s.FiledTypes).Count(&result.Page.PageTotal)
	if query.Error != nil {
		return nil, query.Error
	}

	return result, nil
}

/**
 * 列表
 */
func (s *Source) SourceList(search *BaseSearch) (interface{}, error) {
	if s.Model == nil || s.ModelName == "" {
		return nil, ErrCrudModelNil
	}

	var model = reflect.TypeOf(s.Model)
	sliceT := reflect.SliceOf(model)
	list := reflect.MakeSlice(sliceT, 0, 0).Interface()

	m := s.Database.GetDB(nil).Table(s.ModelName)
	wh := SourceSearchWhere(m, search, s.FiledTypes)
	fields := SourceFieldsFiltter(wh, s.FieldOmits)
	result := fields.Limit(BaseListLimit).Find(&list)

	if result.Error != nil {
		return nil, result.Error
	}
	return list, nil
}

func (s *Source) GetModel() IModel {
	return s.Model
}

func (s *Source) GetModelName() string {
	return s.ModelName
}

// 实例化示例
func NewSource() *Source {
	return &Source{
		ModelName: "source",
		Model:     nil,
		Database:  nil,
	}
}

func SourceSearchWhere(db *gorm.DB, search *BaseSearch, fileds map[string]string) *gorm.DB {
	var conds []interface{}
	sql := ""
	if search == nil {
		return db
	}

	if len(search.FiledsParam) != 0 {
		for _, q := range search.FiledsParam {
			field := utils.StrToSnake(q.Field)
			ftype := fileds[q.Field]

			if ftype != "" {
				switch ftype {
				case FiledEqual:
					// 等于
					sql += " AND " + field + " IN (?)"
					conds = append(conds, q.Value)

				case FiledLike:
					// 模糊查询
					val, ok := q.Value.(string)
					if !ok {
						continue
					}
					sql += " AND " + field + " like ?"
					conds = append(conds, "%"+val+"%")

				case FiledLess:
					// 小于
					val, ok := q.Value.(int)
					if !ok {
						continue
					}
					sql += " AND " + field + " < ?"
					conds = append(conds, val)

				case FiledGreater:
					// 大于
					val, ok := q.Value.(int)
					if !ok {
						continue
					}
					sql += " AND " + field + " > ?"
					conds = append(conds, val)

				default:
					sql += " AND " + field + " IN (?)"
					conds = append(conds, q.Value)
				}
			} else {
				sql += " AND " + field + " IN (?)"
				conds = append(conds, q.Value)
			}
		}
	}

	sql = strings.TrimPrefix(sql, " AND")

	db = db.Where(sql, conds...)

	var od string

	if search.Order != "" {
		od = search.Order
	} else {
		od = "updated_at" // 默认以修改时间排序
	}

	if search.Sort != "" {
		od += " " + search.Sort
	} else {
		// DESC and ASC
		od += " DESC" // 默认升序 DESC
	}

	return db.Order(od)
}

func SourceFieldsFiltter(db *gorm.DB, filter []string) *gorm.DB {
	if filter != nil {
		db.Omit(filter...)
	}
	return db
}
