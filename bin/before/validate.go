package before

import (
	"regexp"

	"github.com/go-playground/validator/v10"
)

var Valid *validator.Validate

/**
 * 已经注册的验证
 * phone：手机号
 * chinese：汉字
 * letter：英文字母
 * stand：标准字符，字母、汉字、中划线、下划线、数字、斜杠、竖线
 * car：车牌号
 */
func RegisterValidate() {
	validate := validator.New()
	validate.RegisterValidation("phone", validatePhone)
	validate.RegisterValidation("chinese", validateChinese)
	validate.RegisterValidation("letter", validateLetter)
	validate.RegisterValidation("stand", validateStand)
	validate.RegisterValidation("car", validateCarId)

	Valid = validate
}

// 手机号验证
func validatePhone(f validator.FieldLevel) bool {
	return validations(f, `^1[3-9]\d{9}$`)
}

// 汉字验证
func validateChinese(f validator.FieldLevel) bool {
	// 等效于 ^[\u4e00-\u9fa5]+$
	return validations(f, `^[\x{4e00}-\x{9fa5}]+$`)
}

// 字母验证
func validateLetter(f validator.FieldLevel) bool {
	return validations(f, `^[A-Za-z]+$`)
}

// 标准字符，允许：字母、汉字、中划线、下划线、数字、斜杠、竖线
func validateStand(f validator.FieldLevel) bool {
	return validations(f, `^[\w\x{4e00}-\x{9fa5}-0-9\\/|]+$`)
}

// 车牌号验证
func validateCarId(f validator.FieldLevel) bool {
	var reg = `^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]+$`
	return validations(f, reg)
}

// 字符串验证的逻辑处理
func validations(f validator.FieldLevel, regStr string) bool {
	field := f.Field().String()
	if len(field) == 0 {
		return true // 为空时不验证
	}
	reg := regexp.MustCompile(regStr)

	if reg.MatchString(field) {
		return true
	} else {
		return false
	}
}
