package basemiddle

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"golang.org/x/time/rate"
)

var limiter = rate.NewLimiter(rate.Limit(120), 10)

/**
 * 限流接口：一秒钟最多120个接口，并行10
 */
func LimitRequest() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if !limiter.Allow() {
			ctx.String(http.StatusTooManyRequests, "too.many.request")
			ctx.Abort()
		} else {
			ctx.Next()
		}
	}
}
