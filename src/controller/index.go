package controller

import (
	"freely/bin/comm"
	controllerfront "freely/src/controller/front"
	"freely/src/middleware"

	"github.com/gin-gonic/gin"
)

// 注册所有路由
func RegisterController() {
	registerControllerToFront()
}

// 注册 front 模块的路由
func registerControllerToFront() {
	var group = comm.Gin.Group("/admin")
	var ignoreList = map[string]bool{
		"/admin/open/login":    true,
		"/admin/open/logout":   true,
		"/admin/open/register": true,
		"/admin/open/captcha":  true,
	}
	group.Use(func(ctx *gin.Context) {
		middleware.TokenAuth(ctx, ignoreList)
	})
	// controllerfront.RegisterControllerFrontDepartment(group.Group("/department"))
	controllerfront.RegisterControllerFrontLogin(group.Group("/open"))
	// controllerfront.RegisterControllerFrontRoles(group.Group("/role"))
	controllerfront.RegisterControllerFrontMenus(group.Group("/menu"))
	controllerfront.RegisterControllerFrontUsers(group.Group("/user"))
}
