package test

import (
	"fmt"
	"freely/test/classes/child"
	"testing"
)

var baseSourceClass = child.NewBaseSourceStu()

func TestMain(t *testing.T) {
	msg := baseSourceClass.SourceSave()
	fmt.Println("message", msg)
}
