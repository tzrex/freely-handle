package modelfront

import (
	"freely/bin/comm"
	"freely/bin/crud"
)

/**
 * 部门表
 */
type FrontDeptStu struct {
	crud.BaseModel
	ParentID uint   `json:"parentId"`
	DeptName string `gorm:"type:varchar(40)" json:"deptName" validate:"required,stand"`
	Status   bool   `json:"status"`
	Remark   string `gorm:"type:varchar(255)" json:"remark"`
}

// 部门表字段情况
var FrontDeptCloumnTypeMap = map[string]string{
	"parentId": crud.FiledEqual,
	"deptName": crud.FiledLike,
	"status":   crud.FiledEqual,
	"remark":   crud.FiledEqual,
}

var FrontDeptName = "front_department"

func (m *FrontDeptStu) TableName() string {
	return FrontDeptName
}

func (m *FrontDeptStu) GroupName() string {
	return ModelName
}

// 迁移部门表的表结构
func MigrateFrontDept() {
	comm.MigrateTable(&FrontDeptStu{})
}
