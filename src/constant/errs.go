package constant

import "errors"

// 日常操作时的常见错误
var (
	ErrInvaild        = errors.New("invalid.operate")
	ErrColumnTypeFail = errors.New("column.type.fail")
	ErrNotFound       = errors.New("not.found")
)

// 登录相关的错误
var (
	ErrToken        = errors.New("token.err")
	ErrCreateToken  = errors.New("token.create.bad")
	ErrTokenExpired = errors.New("token.expired")
	ErrUserLogin    = errors.New("login.userAndPassErr")
	ErrUserExist    = errors.New("login.userExist")
)
