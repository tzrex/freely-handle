### 启动
`golang` 版本：1.20.4

vscode 的 `f5` 按键，或是 `go run main.go`

### 目录构成
* bin：整个项目的基本架构
  * baseMiddle：`gin` 的中间件
  * before：使用的库的封装
  * comm：注册使用 `gin`
  * crud：为快速写业务书写的 `crud` 接口，有 `list`、`page`、`add`、`delete`、`update`、`info` 六个接口
  * customTypes：自定义数据类型
  * utils：常用的方法
* locales：文本数据
* src：业务书写
  * constant：开发时使用的常量
  * controller：业务的API
  * methods：单独的方法
  * middleware：中间件
  * model：定义的数据库结构体
  * router：路由注册
  * source：操作数据库，暴露API
* test：测试

### 数据库

目前只需要登录获取后续的 `token` 就行。

用户信息会存到 `redis` 中，需要开启 `reids`。

登录时有个验证码，这个需要先请求 `/admin/open/captcha` 这个 API

### 遇到的问题

目前书写了 `user` 和 `menus` 两个模块。

在调用 `user` 模块的crudAPI和自定义的API时能正常访问；但是在调用 `menus` 模块的接口时：

在请求经过 `/src/middleware/tokenAuth.go` 中间件时，会报错：

`runtime error: invalid memory address or nil pointer dereference`

### 额外的

还想知道关于 `/bin/crud` 内关于快速生成的API的方式，还有没有优化的地方。