SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;


-- ----------------------------
-- Table structure for front_user
-- ----------------------------
DROP TABLE IF EXISTS `front_user`;
CREATE TABLE `front_user`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) NULL DEFAULT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `username` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `password` varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nick_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `birth` datetime(0) NULL DEFAULT NULL,
  `role_id` bigint(0) UNSIGNED NULL DEFAULT NULL,
  `department_id` bigint(0) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  UNIQUE INDEX `phone`(`phone`) USING BTREE,
  INDEX `idx_front_user_deleted_at`(`deleted_at`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  INDEX `dept_id`(`department_id`) USING BTREE,
  INDEX `email`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of front_user
-- ----------------------------
INSERT INTO `front_user` VALUES (1, '2024-01-23 15:46:57.000', '2024-01-23 15:46:57.000', NULL, 'Rex', 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'Flover', NULL, '', '', '', 1, 1);
INSERT INTO `front_user` VALUES (2, '2024-03-13 10:19:04.000', '2024-03-13 10:19:04.000', NULL, '', 'rex', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '', '', '未绑定-1710296344', '', NULL, 2, 3);
INSERT INTO `front_user` VALUES (3, '2024-03-13 10:21:22.000', '2024-03-13 10:21:22.000', NULL, '', 'flover', 'bd9c92e8d3bbf000caf391391325e0fa26b5355b7156218c82cec0db29da18bd', '', '', '未绑定-1710296482', '', NULL, 2, 3);
INSERT INTO `front_user` VALUES (4, '2024-03-13 10:27:01.000', '2024-03-13 10:27:01.000', NULL, '', 'zhangsan', '866a0cd7226be2998808057993b1d808b4529dac50dd54779525e623ce336fa8', '', '', '未绑定-1710296821', '', NULL, 2, 3);

SET FOREIGN_KEY_CHECKS = 1;


-- ----------------------------
-- Table structure for front_role
-- ----------------------------
DROP TABLE IF EXISTS `front_role`;
CREATE TABLE `front_role`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` datetime(3) NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime(3) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted_at` datetime(3) NULL DEFAULT NULL,
  `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `label` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `label`(`label`) USING BTREE,
  INDEX `idx_front_role_deleted_at`(`deleted_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of front_role
-- ----------------------------
INSERT INTO `front_role` VALUES (1, '2024-01-23 15:49:22.000', '2024-01-23 15:49:22.000', NULL, '管理员', 'admin');
INSERT INTO `front_role` VALUES (2, '2024-01-23 15:50:22.000', '2024-01-23 15:50:22.000', NULL, '游客', 'visitor');

SET FOREIGN_KEY_CHECKS = 1;
