package basemiddle

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

/**
 * 全局捕获panic的中间件
 */
func ExceptionHandler() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				var msg string
				if r, ok := err.(error); ok {
					msg = r.Error()
				} else {
					msg = fmt.Sprint(r)
				}
				ctx.String(http.StatusInternalServerError, msg)
				ctx.Abort()
			}
		}()
		ctx.Next()
	}
}
