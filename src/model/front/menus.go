package modelfront

import (
	"freely/bin/comm"
	"freely/bin/crud"
)

/**
 * 菜单表
 */
type FrontMenuStu struct {
	Path      string          `gorm:"type:varchar(140)" json:"path"`
	Component string          `gorm:"type:varchar(100)" json:"component"`
	Redirect  string          `gorm:"type:varchar(40)" json:"redirect"`
	Name      string          `gorm:"type:varchar(20)" json:"name"`
	ParentID  uint            `json:"parentId"`                     // 父级ID
	Hidden    bool            `json:"hidden"`                       // 是否隐藏
	Icon      string          `gorm:"type:varchar(20)" json:"icon"` // icon图标
	Children  []*FrontMenuStu `gorm:"-" json:"children"`
}

// 菜单表字段属性
var FrontMenuColTypeMap = map[string]string{
	"path":      crud.FiledLike,
	"component": crud.FiledEqual,
	"name":      crud.FiledLike,
	"parentId":  crud.FiledEqual,
	"hidden":    crud.FiledEqual,
}

// 前端所需要的路由结构
type WebFrontSchema struct {
	Path      string              `json:"path"`
	Component string              `json:"component"`
	Redirect  string              `json:"redirect"`
	Name      string              `json:"name"`
	Meta      *WebFrontMetaSchema `json:"meta"`
	Children  []*WebFrontSchema   `json:"children"`
}

// 路由的meta部分
type WebFrontMetaSchema struct {
	Hidden     bool     `json:"hidden"`
	AlwaysShow bool     `json:"alwaysShow"`
	Title      string   `json:"title"`
	Icon       string   `json:"icon"`
	NoCache    bool     `json:"noCache"`
	Breadcrumb bool     `json:"breadcrumb"`
	Affix      bool     `json:"affix"`
	ActiveMenu string   `json:"activeMenu"`
	NoTagsView bool     `json:"noTagsView"`
	CanTo      bool     `json:"canTo"`
	Permission []string `json:"permission"`
}

var FrontMenuName = "front_menus"

func (m *FrontMenuStu) TableName() string {
	return FrontMenuName
}

func (m *FrontMenuStu) GroupName() string {
	return ModelName
}

// 迁移角色的菜单表结构
func MigrateFrontMenu() {
	comm.MigrateTable(&FrontMenuStu{})
}
