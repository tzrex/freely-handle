package comm

import (
	"context"
	"fmt"
	"os"
	"time"

	"freely/bin/before"

	"github.com/gin-gonic/gin"
	"github.com/redis/go-redis/v9"
	"gorm.io/gorm"
)

var (
	Gin      *gin.Engine
	Db       *before.DatabaseConnect
	Mysql    *gorm.DB
	RedisCli *redis.Client
)

var (
	dbPath    string
	redisPath string
)

var AppCtx = context.Background()

// 注册模板
func init() {
	initGin()   // 注册 gin
	initMysql() // 注册mysql
	initRedis() // 注册redis
	initValid() // 注册字段验证
}

func initMysql() {
	// 连接mysql
	Mysql = before.NewConnectionDatabase(&before.ConnectConfigStu{
		DbType:         "Mysql",
		DbHost:         dbPath,
		DbPort:         "3306",
		DbUsername:     "demo",
		DbPassword:     "cfP3S4t5sXnmNJwT",
		DbName:         "demo",
		DbMaxOpenConns: 100,
		DbMaxIdleConns: 10,
		DbMaxLifetime:  time.Hour * 2,
	})

	if Mysql == nil {
		fmt.Println("mysql链接失败")
	} else {
		fmt.Println("mysql.success")
	}

	Db = before.NewDatabaseConnect(Mysql)
}

func initRedis() {
	// 连接redis
	RedisCli = before.NewConnectionRedis(&before.RedisConnetConfigStu{
		RedisHost:   redisPath,
		RedisPort:   "6379",
		RedisPass:   "",
		DialTimeout: time.Duration(30) * time.Minute,
		ReadTimeout: time.Duration(30) * time.Minute,
	})

	str, err := RedisCli.Ping(AppCtx).Result()
	if err != nil {
		fmt.Println("redis连接失败")
	} else {
		fmt.Println("redis.ping", str)
	}
}

func initGin() {
	var isProd bool

	if os.Getenv("env") == "production" {
		isProd = true
		// 改为对应的容器服务名称
		dbPath = "mysql"
		redisPath = "redis"
	} else {
		isProd = false
		dbPath = "127.0.0.1"
		redisPath = "127.0.0.1"
	}

	// 添加 gin
	Gin = before.CreateGin(&before.GinSchema{
		IsProd:        isProd,
		SessionKey:    "freely-session",
		SessionSecret: "rex-fwfh54g2w45g12s4",
	})

	fmt.Println("[ isProd ]: ", isProd)
}

func initValid() {
	// 注册validate验证
	before.RegisterValidate()
}
