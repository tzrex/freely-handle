package before

import (
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// 数据库接口
type IDatabaseConnect interface {
	GetDB(*gorm.DB) *gorm.DB
}

// 数据库实例
type DatabaseConnect struct {
	Database *gorm.DB
}

// gorm 连接配置
type ConnectConfigStu struct {
	DbType         string        // 数据库类型：mysql、mssql、oracle
	DbHost         string        // 连接主体：127.0.0.1
	DbPort         string        // 端口号
	DbUsername     string        // 数据库用户账号
	DbPassword     string        // 数据库用户密码
	DbName         string        // 数据库名称
	DbMaxOpenConns int           // 最大连接数
	DbMaxIdleConns int           // 最大空闲连接数量
	DbMaxLifetime  time.Duration // 最大生存时间
}

/**
 * 数据库连接
 * @Return 是否连接成功
 */
func NewConnectionDatabase(config *ConnectConfigStu) *gorm.DB {
	switch config.DbType {
	case "Mysql":
		return connectMysql(config)
	// case "Mssql":
	// 	return connectMssql(config)
	default:
		return nil
	}
}

/**
 * 连接Mysql数据库
 */
func connectMysql(config *ConnectConfigStu) *gorm.DB {
	driverDns := config.DbUsername + ":" + config.DbPassword +
		"@tcp(" + config.DbHost + ":" + config.DbPort + ")/" +
		config.DbName +
		"?charset=utf8mb4&parseTime=True&loc=Local"

	db, err := gorm.Open(
		mysql.Open(driverDns),
		&gorm.Config{
			PrepareStmt: true,
		},
	)
	if err != nil {
		return nil
	}

	sqlDB, err := db.DB()
	if err != nil {
		return nil
	}

	sqlDB.SetMaxOpenConns(config.DbMaxIdleConns)

	sqlDB.SetMaxIdleConns(config.DbMaxIdleConns)

	sqlDB.SetConnMaxLifetime(config.DbMaxLifetime)

	return db
}

/**
 * 获取数据库，参数为当在事务中使用是传递
 */
func (db *DatabaseConnect) GetDB(tx *gorm.DB) *gorm.DB {
	var table *gorm.DB
	if tx != nil {
		table = tx
	} else {
		table = db.Database
	}
	return table.Session(&gorm.Session{
		NewDB:           true,
		PrepareStmt:     true,
		CreateBatchSize: 500,
	})
}

// 实例化 IDatabaseConnect 接口
func NewDatabaseConnect(db *gorm.DB) *DatabaseConnect {
	return &DatabaseConnect{
		Database: db,
	}
}
