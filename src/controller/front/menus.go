package controllerfront

import (
	"freely/bin/before"
	"freely/bin/crud"
	modelfront "freely/src/model/front"
	sourcefront "freely/src/source/front"
	"net/http"

	"github.com/gin-gonic/gin"
)

type ControllerFrontMenus struct {
	*crud.Controller
}

var frontMenusClass *sourcefront.SourceFrontMenus

func init() {
	frontMenusClass = sourcefront.NewSourceFrontMenus()
}

/**
 * 获取角色相关的菜单
 */
func (c *ControllerFrontMenus) RoleMenus(ctx *gin.Context) {
	search := &crud.BaseSearch{
		FiledsParam: []*crud.QueryFild{
			{Field: "roleId", Value: ""},
		},
	}
	list, err := frontMenusClass.SourceList(search)
	if err != nil {
		ctx.JSON(http.StatusOK, before.ReqFail(ctx, err.Error()))
		return
	}
	ctx.JSON(http.StatusOK, before.ReqOk(ctx, list))
}

func RegisterControllerFrontMenus(router *gin.RouterGroup) {
	var apis = []string{"/add", "/update", "/delete", "/list", "/info"}

	var controller = ControllerFrontMenus{
		&crud.Controller{
			Module: modelfront.ModelName,
			Source: frontMenusClass,
		},
	}

	var extendApi = map[string]*crud.RouterItem{
		"/roleMenus": {From: crud.RouterPost, Call: controller.RoleMenus},
	}

	crud.RegisterController(router, controller, apis, extendApi)
}
