package basemiddle

import (
	"encoding/json"

	ginI18n "github.com/gin-contrib/i18n"
	"github.com/gin-gonic/gin"
	"golang.org/x/text/language"
)

/**
 * 多语言配置
 * 可通过 header[Accept-Language] 进行配置
 * en：英文
 * zh：中文
 */
func I18nHandle() gin.HandlerFunc {
	var bundle = ginI18n.WithBundle(&ginI18n.BundleCfg{
		DefaultLanguage:  language.Chinese,
		FormatBundleFile: "json",
		UnmarshalFunc:    json.Unmarshal,
		RootPath:         "./locales/",
		AcceptLanguage: []language.Tag{
			language.Chinese,
			language.English,
		},
	})

	return ginI18n.Localize(bundle)
}
